OCP is hammering the SGDS DNS with requests for cognito-idp.eu-central-1.amazonaws.com. This makes the DNS operators upset and they want us to stop.

cognito-idp is an Amazon IAM service used for logging customers in to the different webshops.

Example log from the DNS:
```bash
Dec 15 09:27:32 10.154.0.21 named[23967]: client @0x7f09f5b7e720 10.99.68.230#33328 (cognito-idp.eu-central-1.amazonaws.com): query: cognito-idp.eu-central-1.amazonaws.com IN A +E(0) (10.255.255.10)
```
I ran tcpumdp on the worker node and you can see bursts of queries:
```bash
tcpdump -i any udp port 53 | grep idp
15:17:02.415147 IP 172.28.6.11.53464 > ns1.sgt.saint-gobain.net.domain: 61919+ [1au] A? cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se. (89)
15:17:02.415308 IP 172.28.6.11.46540 > ns1.sgt.saint-gobain.net.domain: 17080+ [1au] A? cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se. (89)
```
How can this be?

The resolv.conf in the container looks like this:
```bash
search dahl-dev.svc.cluster.local svc.cluster.local cluster.local stratus.cloud.dahl.se
nameserver 172.16.0.10
options ndots:5
```
Which explains why it asks for cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se before asking for cognito-idp.eu-central-1.amazonaws.com. This is how name resolution works in Kubernetes so it's nothing strange.

The OCP DNS setup shows why queries for dahl.se goes to the internal SGDS DNS:
```bash
# int
    dahl.se:5353 {
        forward . 10.255.255.10
        errors
        bufsize 512
    }
```
My first idea was to increase the TTL for this domain and cache nxdomain answers for an hour but the OCP DNS Operator doesn't support setting TTLs (yet): https://access.redhat.com/solutions/5537961

Then I tried to route queries for cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se to nothing:
```bash
# blocklist
    cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se:5353 {
        forward . 0.0.0.0
        errors
        bufsize 512
    }
```

But that broke DNS resolution for this domain completely. Why? Because there is a bug in the Alpine Linux resolver: https://stackoverflow.com/questions/65181012/does-alpine-have-known-dns-issue-within-kubernetes. It doesn't seem to like "refused" or "servfail" as answers. If it gets any of those it will simply stop doing lookups and thus never ask for the correct domain. And the auth service container is of course based on Alpine...

So I deployed dnsmasq as an authorative name server for stratus.cloud.dahl.se which will return nxdomain when asked for cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se. This should work as nxdomain is an answer the resolver in Alpine accepts and it should continue down the list of domains to ask for eventually asking for the correct one.
```bash
 # blocklist
    cognito-idp.eu-central-1.amazonaws.com.stratus.cloud.dahl.se:5353 {
        forward . 172.17.232.0
        errors
        bufsize 512
    }
```
NB: forward must point to an IP.

The team responsible for the auth service are aware of this and looking into fixing the code.
